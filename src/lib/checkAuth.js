import { firebaseConfig } from '../services/firebase'

let refreshToken = async () => {
    const res = await fetch(`https://securetoken.googleapis.com/v1/token?key=${firebaseConfig.apiKey}`, {
        method: 'POST',
        body: JSON.stringify({
            "grant_type": "refresh_token",
            "refresh_token": userData.stsTokenManager.refreshToken
        })
    })
    
    const json = await res.json()
    console.log(JSON.stringify(json))
} 

let checkAuth = async () => {
    let userData = JSON.parse(localStorage.getItem('user'))
    console.log(userData)
    if (!userData.uid) goto("/")
}

export { checkAuth }
