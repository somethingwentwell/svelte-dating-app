import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";

const firebaseConfig = {
    apiKey: "FIREBASE_API_KEY",
    authDomain: "svelte-chat-ef59e.firebaseapp.com",
    databaseURL: "https://svelte-chat-ef59e-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "svelte-chat-ef59e",
    storageBucket: "svelte-chat-ef59e.appspot.com",
    messagingSenderId: "798247171811",
    appId: "1:798247171811:web:b4aa7a66cb20eb1d7a37d6",
    measurementId: "G-9NT7E3NHKP"
  };

const app = initializeApp(firebaseConfig)
const db = getDatabase(app)

export { firebaseConfig, app, db }