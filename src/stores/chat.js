import { writable } from 'svelte/store';
import { ref, onValue, push } from "firebase/database";
import { db } from '../services/firebase'


const messages = writable([]);

let getMessage = (other, me) => {
    console.log(`chat/${other}/${me}/`)
    onValue(ref(db, `chat/${other}/${me}/`), (snapshot) => {
        console.log(snapshot.val())
        messages.update(data => snapshot.val());
      });    
}

let sendMessage = (message, other, me, reverse) => {
  let fromUser = me;
  if (reverse) {
    fromUser = other
  }
  push(ref(db, `chat/${other}/${me}/`), {
    content: message,
    from: fromUser,
    time: Math.floor(Date.now() /1000)
  });
}

export {messages, getMessage, sendMessage};