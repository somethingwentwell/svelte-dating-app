import { writable } from 'svelte/store';
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { app } from '../services/firebase'
import { goto } from '$app/navigation';

const blankUser = {
    "name": "",
    "feed": "",
    "maxReq": 3,
    "curReq": 0,
    "googleUid": "",
    "userInChat": {}
}

const user = writable(blankUser)

let checkUser = async () => {
    let auth = getAuth(app)
    onAuthStateChanged(auth, (firebaseUser) => {
            user.update((data) => {
                data.googleUid = firebaseUser.uid;
                return data
            });
            if (!firebaseUser) {
                console.log("logout")
            }
        })
}

export {user, blankUser, checkUser};