import { writable } from 'svelte/store';
import { ref, onValue, set } from "firebase/database";
import { db } from '../services/firebase'

const feeds = writable([]);

let getFeeds = () => {
    onValue(ref(db, 'feed/'), (snapshot) => {
        feeds.update(data => snapshot.val());
      });    
}

function updateFeed(userId, topic) {
  console.log(userId);
  console.log(topic);
  set(ref(db, 'feed/' + userId), {
    content: topic,
    gender: "male"
  });
}

export {feeds, getFeeds, updateFeed};