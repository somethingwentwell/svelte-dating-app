import { writable } from 'svelte/store';
import { ref, onValue, orderByChild, equalTo, set } from "firebase/database";
import { db } from '../services/firebase'
import { user } from './user';


const list = writable([]);

let getList = (me) => {
    onValue(ref(db, `user/${me}`), (snapshot) => {
        user.update((data) => {

          if (snapshot.val().request != undefined) {
            if (Object.keys(snapshot.val().request).length > 0) {
              data.curReq = Object.keys(snapshot.val().request).length;
            }
          }
          console.log(snapshot.val().request)
          return data
        });
        list.update(data => snapshot.val());
      })
}

function addReq(me, userId, topic, myTopic) {
    set(ref(db, `user/${me}/request/${userId}`), {
      content: topic
    });
    set(ref(db, `user/${userId}/friendReq/${me}`), {
      content: myTopic
    });
}

function cancelReq(me, userId) {
  set(ref(db, `user/${me}/request/${userId}`), null);
  set(ref(db, `user/${userId}/friendReq/${me}`), null);
}

function acceptFrdReq(me, userId, topic) {
  set(ref(db, `user/${me}/match/${userId}`), {
    content: topic,
    initByMe: false
  });
  set(ref(db, `user/${userId}/match/${me}`), {
    content: topic,
    initByMe: true
  });
  set(ref(db, `user/${me}/friendReq/${userId}`), null);
  set(ref(db, `user/${userId}/request/${me}`), null);
}

function cancelFrdReq(me, userId) {
  set(ref(db, `user/${me}/friendReq/${userId}`), null);
  set(ref(db, `user/${userId}/request/${me}`), null);
}

export {list, getList, addReq, cancelReq, acceptFrdReq, cancelFrdReq};