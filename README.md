# Svelte Dating App

Start App

```
npm install
npm run dev
```

Deploy as static app

In svelte.config.js

```
// svelte.config.js
import adapter from '@sveltejs/adapter-static';

export default {
	kit: {
		adapter: adapter({
			// default options are shown
			pages: 'build',
			assets: 'build',
			fallback: null
		})
	}
};
```


```
npm i -D @sveltejs/adapter-static@next
npm run build
```